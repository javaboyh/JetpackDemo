# JetpackDemo

#### 项目介绍
Google 2018 I/O大会上发布了Jetpack框架。影响深远。

![输入图片说明](https://images.gitee.com/uploads/images/2018/1210/151923_b45beca9_2145149.jpeg "jetpack.jpg")

本项目将介绍Jetpack的使用。

#### 安装教程

1. 使用Android Studio，Check out project from Version Control。
2. 或者使用git clone命令到本地，用Android Studio打开。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
