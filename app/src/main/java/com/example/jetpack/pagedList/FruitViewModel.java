package com.example.jetpack.pagedList;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.example.jetpack.bean.FruitBean;
import com.example.jetpack.datasource.DataSourceFactory;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class FruitViewModel extends ViewModel {
    private Executor myExecutor = Executors.newSingleThreadExecutor();

    //通过Config 设置PageList 从Datasource 加载数据的方式。
    private PagedList.Config myPagingConfig = new PagedList.Config.Builder()
            .setInitialLoadSizeHint(20)//设置首次加载的数量；
            .setPageSize(10)//设置每一页加载的数量；
            .setPrefetchDistance(10)//设置距离最后还有多少个item时加载下一页；
            .setEnablePlaceholders(false)//表示是否设置null占位符；
            .build();

    private DataSource.Factory<Integer, FruitBean> myConcertDataSource =
            new DataSourceFactory();

    public LiveData<PagedList<FruitBean>> getConcertList() {
        return concertList;
    }

    private LiveData<PagedList<FruitBean>> concertList =
            new LivePagedListBuilder<>(myConcertDataSource, myPagingConfig)
                    .setFetchExecutor(myExecutor)//设置Executor执行器的线程。
                    .build();

    /**
     * 数据刷新方法
     * 由于利用了MutableLiveData机制，concertList能响应数据变化。
     */
    public void invalidateDataSource() {
        PagedList<FruitBean> pagedList = concertList.getValue();
        if (pagedList != null){
            pagedList.getDataSource().invalidate();
        }
    }
}