package com.example.jetpack.pagedListAdapter;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jetpack.R;
import com.example.jetpack.bean.FruitBean;

public class FruitAdapter extends PagedListAdapter<FruitBean, FruitAdapter.FruitViewHolder> {
    private Context context;

    public FruitAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    @NonNull
    @Override
    public FruitViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fruit, viewGroup, false);
        return new FruitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FruitViewHolder viewHolder, int position) {
        FruitBean dataBean = getItem(position);
        if (dataBean == null) {
            viewHolder.id.setText("xxx");
            viewHolder.name.setText("佚名");
            viewHolder.image.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_launcher));
        } else {
            viewHolder.id.setText(String.valueOf(dataBean.getId()));
            viewHolder.name.setText(dataBean.getName());
            viewHolder.image.setImageDrawable(context.getResources().getDrawable(dataBean.getImage()));
        }
    }

    class FruitViewHolder extends RecyclerView.ViewHolder {
        TextView id;
        TextView name;
        ImageView image;

        FruitViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.tv_id);
            name = itemView.findViewById(R.id.fruit_name);
            image = itemView.findViewById(R.id.fruit_image);
        }
    }

    private static DiffUtil.ItemCallback<FruitBean> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<FruitBean>() {
                @Override
                public boolean areItemsTheSame(FruitBean oldItem, FruitBean newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(FruitBean oldItem, @NonNull FruitBean newItem) {
                    return oldItem.equals(newItem);
                }
            };

}
