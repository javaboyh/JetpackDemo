package com.example.jetpack.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.paging.PagedList;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.jetpack.bean.FruitBean;
import com.example.jetpack.R;
import com.example.jetpack.pagedList.FruitViewModel;
import com.example.jetpack.pagedListAdapter.FruitAdapter;

public class PageActivity extends AppCompatActivity {
    private RecyclerView mRvMain;
    private SwipeRefreshLayout mRefresh;

    private FruitAdapter mAdapter;
    private FruitViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_j);
        mRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mRvMain = (RecyclerView) findViewById(R.id.rv_main);

        mRvMain.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new FruitAdapter(this);
        mRvMain.setAdapter(mAdapter);
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        mRvMain.addItemDecoration(divider);

        //将数据与ViewModelProvider构造方法第一个参数传入的对象生命周期绑定。
        ViewModelProvider provider = new ViewModelProvider(this,
                new ViewModelProvider.AndroidViewModelFactory(getApplication()));
        mViewModel = provider.get(FruitViewModel.class);
        //将数据(既被观察者)订阅给观察者。
        mViewModel.getConcertList().observe(this, new Observer<PagedList<FruitBean>>() {
            @Override
            public void onChanged(@Nullable PagedList<FruitBean> dataBeans) {
                mAdapter.submitList(mViewModel.getConcertList().getValue());
            }
        });
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mViewModel.invalidateDataSource();
                mRefresh.setRefreshing(false);
            }
        });
    }
}
