package com.example.jetpack.datasource;

import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;

import com.example.jetpack.bean.FruitBean;
import com.example.jetpack.R;

import java.util.ArrayList;
import java.util.List;

public class FruitDataSource extends PositionalDataSource<FruitBean> {
    private int[] imageArray = new int[]{R.drawable.apple_pic, R.drawable.banana_pic,
            R.drawable.cherry_pic, R.drawable.grape_pic, R.drawable.mango_pic,
            R.drawable.orange_pic, R.drawable.pear_pic, R.drawable.pineapple_pic,
            R.drawable.strawberry_pic, R.drawable.watermelon_pic};
    private String[] nameArray = new String[]{"apple", "banana",
            "cherry", "grape", "mango",
            "orange", "pear", "pineapple",
            "strawberry", "watermelon"};

    /**
     * 初始化时加载内容。
     * @param params
     * @param callback
     */
    @Override
    public void loadInitial(@NonNull LoadInitialParams params,
                            final @NonNull LoadInitialCallback<FruitBean> callback) {
        final int startPosition = 0;
        List<FruitBean> list = buildDataList(startPosition, params.requestedLoadSize);
        //将数据回调
        callback.onResult(list, 0);
    }

    /**
     * 加载指定范围的数据。
     * @param params
     * @param callback
     */
    @Override
    public void loadRange(@NonNull final LoadRangeParams params,
                          @NonNull final LoadRangeCallback<FruitBean> callback) {
        List<FruitBean> list = buildDataList(params.startPosition, params.loadSize);
        callback.onResult(list);
    }

    private List<FruitBean> buildDataList(int startPosition, int loadSize) {
        List<FruitBean> list = new ArrayList<>();
        FruitBean bean;
        for (int i = startPosition; i < startPosition + loadSize; i++) {
            bean = new FruitBean();
            bean.setId(i);
            bean.setImage(imageArray[i % imageArray.length]);
            bean.setName(nameArray[i % nameArray.length]);
            list.add(bean);
        }
        return list;
    }
}
