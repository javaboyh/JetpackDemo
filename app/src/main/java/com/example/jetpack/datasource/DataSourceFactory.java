package com.example.jetpack.datasource;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.example.jetpack.bean.FruitBean;

/**
 * 这是LiveData提供的：数据变化时，可以动态更新数据的机制。
 */
public class DataSourceFactory extends DataSource.Factory<Integer, FruitBean> {
    private MutableLiveData<FruitDataSource> mSourceLiveData =
            new MutableLiveData<>();

    @Override
    public DataSource<Integer, FruitBean> create() {
        FruitDataSource source = new FruitDataSource();
        mSourceLiveData.postValue(source);
        return source;
    }
}