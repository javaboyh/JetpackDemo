package com.example.jetpack.bean;

public class FruitBean {
    private int id;
    private int image;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean equals(FruitBean o) {
        return id == o.getId();
    }
}
